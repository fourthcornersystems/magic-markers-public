# README #

### Overview ###

* Magic Markers -- Asset for Unity game engine
* 20171124

### Documentation ###

* Detailed documentation is in the folder Assets/Plugins/MagicMarkers/Documentation/.
* See the file CHANGELOG.md for release history.

### Supported Unity Versions ###

* 2017.3
* 2017.2
* 2017.1
* 5.6
* 5.5
* 5.4

Magic Markers may work with older versions of Unity, or with the latest beta version, but only the above versions are officially supported.

### Supported Runtime Targets ###

* Full runtime support on Windows, OSX, and Linux with Shader Model 3.0 or later (Shader Model 4.0 or later required for wireframe feature).
* Magic Markers probably works on other SM 3.0 and later platforms but has not been tested there.
* Please inform the author of your test results on other runtime targets so the documentation can be updated.

### Installation ###

* Import the Magic Markers Unity Package into your project. The code will reside under Assets/Plugins/MagicMarkers/.
* Do not import with outstanding script compilation errors, or Magic Markers will not work correctly.
* The custom shader resides in Assets/Plugins/MagicMarkers/Resources. It can be moved, but the path must include the string "/Resources/" so Unity will include the file with your builds.
* The asset files for marker types (class MagicMarkers.MagicMarkerType and subclasses) can be moved anywhere in your project folder tree.

### Removal ###

* Delete all MagicMarker components from objects in your project's scenes and from prefabs.
* Delete the files from the Magic Markers package.

### Upgrades ###

* Make a backup of your Unity project before you upgrade this or any other third-party asset.
* Unless noted otherwise in the Release Notes, upgrades can generally be done by importing the new package over the old one.

### License ###

* Magic Markers asset is licensed under the terms of the Unity Technologies Asset Store License as an Editor Extension.
* One license is required per developer seat.
* MagicMarkerType assets that you create (but not the C# code that ships with Magic Markers) can be shared as you wish.

### Who do I talk to? ###

* Developer: Scott Courtney FourthCornerSystems@gmail.com (username Syscrusher on Unity forums)
